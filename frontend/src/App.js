import React from 'react';
import MuiThemeProvider from '@material-ui/styles/ThemeProvider';
import './App.css';
import {theme} from './lib/theme'
import AppRouter from "./routes";

function App() {
  return (
      <MuiThemeProvider theme={theme}><AppRouter/></MuiThemeProvider>
  );
}

export default App;
