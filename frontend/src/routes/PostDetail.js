import React, {useState} from "react";
import {getCookie} from "../lib/auth";
import {Container} from "@material-ui/core";
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";
import Post from "../components/Post";
import {CommentListing} from "../components/CommentListing";

export function PostDetail ({match}) {
    const [isAuthUser] = useState(getCookie('JWT'));
    const post_id = match.params.id
    const [{ data, isLoading}, doFetch] = useDataApi(
        '',
        null,
        null,
        true
    );
    const [didFetch, setDidFetch] = useState(false);
    if(!didFetch){
        setDidFetch(true);
        doFetch(API.API_URLS.post_list + `${post_id}/`);
    }
    if(!isAuthUser) return null;
    /**
     * {id, comments, owner, votes, created_at, edited_at, up_vote_count, down_vote_count, content, editor}
     */
    console.log(data)
    return (
        <Container maxWidth={'lg'}>
            {isLoading
            ? 'Loading....'
            : <>{data && <>
                    <Post wide data={data}/>
                    {data.comments && <CommentListing comments={data.comments} postId={data.id}/>}
            </>}</>}
        </Container>
    )
}
