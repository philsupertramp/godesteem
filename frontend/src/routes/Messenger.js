import React, {useState} from 'react';

import {Container} from "@material-ui/core";
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";
import {ThreadListing} from "../components/ThreadListing";
import {Thread} from "../components/Thread";

export function Messenger () {
  const [user, setUser] = useState(null);
  const [thread, setThread] = useState(null);
  const [{ data, isLoading, isError }] = useDataApi(
    API.API_URLS.thread_list,
    null,
    null,
    false
  );
  if(isError) return null

  if(data && !user) setUser(data)
  return (
    <Container maxWidth={'lg'}>
      {user && !isLoading ? <ThreadListing data={user} onClick={(e) => setThread(e)}/> : 'Loading ...'}
      {thread && <Thread threadObj={thread}/>}
    </Container>
  )
}
