import React from "react";
import { BrowserRouter as Router, Route} from "react-router-dom";
import NavBar from "../components/NavBar";
import {Home} from "./Home";
import {User} from "./User";
import {Messenger} from "./Messenger";
import {Sidekicks} from "./Sidekicks";
import {PostDetail} from "./PostDetail";


function AppRouter() {
  return (
    <Router>
      <div>
        <NavBar/>

        <Route path="/" exact component={Home} />
        <Route path="/posts/:id" exact component={PostDetail}/>
        <Route path="/users/:id" exact component={User} />
        <Route path="/messenger" exact component={Messenger}/>
        <Route path="/sidekicks" exact component={Sidekicks}/>
      </div>
    </Router>
  );
}

export default AppRouter;
