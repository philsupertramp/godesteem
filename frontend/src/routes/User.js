import React, {useState} from 'react';

import {Container} from "@material-ui/core";
import {UserDetail} from "../components/UserDetail";
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";

export function User ({ match }) {
  const [user, setUser] = useState(null);
  const user_id = match.params.id
  const [{ data, isLoading, isError }, fetchData] = useDataApi(
    API.API_URLS.user_list + `${user_id}/`,
    null,
    null,
    true
  );
  if(isError) {
    console.log('ERR')
    return null
  }
  if(data && !user) {
    console.log('22', data)
    setUser(data)
  }
  if(data !== user && data && user && user_id !== user.id && user_id !== 'me'){
    setUser(null);
    console.log('27', data)
    fetchData(API.API_URLS.user_list + `${user_id}/`);
  }
  return (
    <Container maxWidth={'lg'}>
      {user && !isLoading ? <UserDetail disabled={user_id !== 'me'} user={user} setUser={(user) => setUser(user)} /> : 'Loading ...'}
    </Container>
  )
}