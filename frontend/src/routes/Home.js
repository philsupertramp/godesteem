import React, {useState} from 'react';
import {Container} from "@material-ui/core";
import {PostListing} from "../components/PostListing";
import {getCookie} from "../lib/auth";
import {Landingpage} from "../components/Home/Landingpage";

export function Home () {
  const [isAuthUser] = useState(getCookie('JWT'));
  return (
    <Container maxWidth={'lg'}>
      {isAuthUser
          ? <PostListing/>
          : <Landingpage/>
      }
    </Container>
  )
}
