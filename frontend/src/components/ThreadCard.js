import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import ButtonBase from '@material-ui/core/ButtonBase';

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    width: '25rem',
    marginBottom: theme.spacing(1)
  },
  details: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flex: '1 0 auto',
    width: '18.5rem'
  },
  cover: {
    width: '5rem',
    height: '5rem',
    margin: theme.spacing(1)
  },
  fullName: {
    width: '12.5rem',
    float: 'left'
  },
  actionButtons: {
    maxWidth: '7.5rem',
    float: 'right',
  }
}));

export function ThreadCard({data, onClick}) {
  const classes = useStyles();
  const userLinkInactive = !!onClick;
  return (
    <Card className={classes.card}>
      <ButtonBase onClick={() => onClick ? onClick({id: data.id}) : null} >
        <Link to={userLinkInactive ? {} : `/users/${data.id}`}>
          <CardMedia
            component={Avatar}
            className={classes.cover}
            image={data.profile_image_url}
            title={data.name}
          />
        </Link>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <div className={classes.fullName}>
              <Typography component="h5" variant="h5">
                <Link to={userLinkInactive ? {} : `/messenger/${data.id}`} style={{textDecoration: 'none'}}>
                  <Typography variant={'h6'} color={'primary'}>{data.name}</Typography>
                </Link>
              </Typography>
              {/* TODO: add unread messages! */}
            </div>
          </CardContent>
        </div>
      </ButtonBase>
    </Card>
  )
}