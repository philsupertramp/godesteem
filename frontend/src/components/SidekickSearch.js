import React, {useEffect, useState} from "react";
import AsyncSelect from 'react-select/async';
import {api, API_URLS} from "../lib/api";
import {Notification} from "./common/Notification";

function cleanResults(resultArray) {
  return resultArray.map((e) => {
    return {label: e.username, value: e.id}
  })
}

async function fetchData(query){
  const response = await api(API_URLS.user_list + `?search=${query}`)
  if(response.data){
    return cleanResults(response.data.results)
  }
}

async function requestUser(user){
  if(user) {
    const response = await api.post(API_URLS.user_list + `${user.value}/sidekick-request/`)
    if (response.data) {
      return {...response.data, status: response.status}
    }
  }
}

export function SidekickSearch () {
  const [selectedUser, setSelectedUser] = useState(null)
  const [requestStatus, setRequestStatus] = useState(null)

  const [notificationOpen, setNotificationOpen] = useState(false)

  useEffect( () => {
    async function fetch(){
      const response = await requestUser(selectedUser)
      setRequestStatus(response)
      setNotificationOpen(true)
    }
    fetch();
  }, [selectedUser])

  return (<>
    {requestStatus && <Notification variant={requestStatus.status === 201 ? 'success' : 'info'}
        open={notificationOpen} handleOpen={setNotificationOpen}>{requestStatus.message}</Notification>}
    <form>
        <AsyncSelect
          cacheOptions
          value={selectedUser}
          onChange={setSelectedUser}
          loadOptions={fetchData}
        />
    </form>
  </>)
}