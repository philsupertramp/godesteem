import React, {useState} from 'react';
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";
import {UserCard} from "./UserCard";
import {AcceptSidekickRequestButton} from "./common/Buttons";
import {Notification} from "./common/Notification";

export function SidekickRequests() {
  const [requests, setRequests] = useState(null)
  const [acceptRequestId, setAcceptRequestId] = useState(null)
  const [requestStatus, setRequestStatus] = useState(null)
  const [notificationOpen, setNotificationOpen] = useState(false)
  const [{ data, isError }, fetchData, setPayload] = useDataApi(
    API.API_URLS.sidekick_request_list,
    null,
    null,
    false
  );
  if(isError) {
    console.log('ERR')
    return null
  }

  if(acceptRequestId){
    setPayload({})
    fetchData(API.API_URLS.sidekick_request_list + `${acceptRequestId}` + API.API_URLS.sidekick_request_accept)
    setAcceptRequestId(null)
  }
  if(!requestStatus && data && data.message) {
    setRequestStatus({message: data.message, status: 200})
    setNotificationOpen(true)
    setTimeout(() => {
      setRequestStatus(null)
    }, 6000)
  }

  if(!requests && data && typeof data === typeof []){
    setRequests(data)
  }
  return (<div style={{marginTop: '5rem'}}>
      {requestStatus && <Notification variant={requestStatus.status === 201 ? 'success' : 'info'}
        open={notificationOpen} handleOpen={setNotificationOpen}>{requestStatus.message}</Notification>}
    {requests && requests.map((obj) => (
      <UserCard key={obj.id} data={obj} username={obj.author.username} button={<AcceptSidekickRequestButton requestId={obj.id} onClick={(e) => setAcceptRequestId(e)}/>}/>
    ))}
  </div>
  )
}