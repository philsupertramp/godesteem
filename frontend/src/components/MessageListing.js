import React from 'react';
import {Paper} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import CheckIcon from "@material-ui/icons/Check";
import CrossIcon from "@material-ui/icons/Clear";
import {decryptMessage} from "../lib/encryption";
import {displayDate, displayTime} from "../lib/helper";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 1),
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
}));

function ReadStatus({read}){
  return (
    <div style={{maxWidth: '10%', float: 'right'}} title={read ? 'Read' : 'Unread'}>{read ? <CheckIcon /> : <CrossIcon/>}</div>
  )
}

function Message({message}) {
  const classes = useStyles();
  return (
    <Paper className={classes.root} style={{float: message.from_me ? 'right' : 'left', backgroundColor: message.from_me ? '#b1c9ff' : '#a4ff98', width: '80%'}}>
      <Typography>{decryptMessage(message.content)}</Typography>
      <Typography style={{float: 'right'}}>
        <div style={{float: 'left', maxWidth: '90%'}}>{displayDate(message.created_at)} {displayTime(message.created_at)}</div>
        <ReadStatus read={message.is_read}/>
      </Typography>
    </Paper>
  )
}


export function MessageListing({messages}){
  return (<div style={{marginTop: '2.5rem', float: 'left', width: '40rem', height: '25rem', overflowY: 'auto'}} id={'message-list'}>
    {messages && messages.map((obj) => (
      <Message key={obj.id} message={obj}/>
    ))}
  </div>)
}
