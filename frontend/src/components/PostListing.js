import React, {useState} from "react";
import Post from "./Post";
import Typography from "@material-ui/core/Typography";
import {API} from "../lib/api";
import {useDataApi} from "../lib/hooks";
import Fab from "@material-ui/core/Fab";
import PostModal from "./PostModal";
import {makeStyles} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
        position: 'absolute',
        right: '0.25rem',
        bottom: '0.25rem'
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

export function PostListing(){
    const classes = useStyles();
    const [postModalOpen, setPostModalOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    let [posts, setPosts] = useState(null);
    const [{ data, isLoading}, doFetch] = useDataApi(
        '',
        null,
        null,
        false
    );

    function togglePostModal(){
        setPostModalOpen(!postModalOpen)
    }
    function addPost(post){
        let doAdd = true;
        posts.forEach((elem) => {
            if(elem.id === post.id) doAdd = false;
        });
        if(doAdd) {
            window.location.reload();
        }
    }
    if(!posts && !data && !loading) {
        doFetch(API.API_URLS.post_list);
        setLoading(true);
    }
    if(!posts && data){
        setPosts(data);
        setLoading(false);
    }
    if(posts && data && posts.length < data.length)
        setPosts(data);
    if(isLoading)
        return (<>
            Loading...
        </>);
    return (
        <>
            {posts && posts.map((elem) => (
                <Post key={elem.id} data={elem}/>
            ))}
            <Fab onClick={() => togglePostModal()} color="primary" aria-label="add" className={classes.fab}>
                <AddIcon />
            </Fab>
            <PostModal openModal={postModalOpen} handleClose={() => setPostModalOpen(false)} addPost={(post) => addPost(post)}/>
        </>
    )
}
