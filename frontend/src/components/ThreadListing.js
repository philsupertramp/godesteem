import React from 'react';
import FormLabel from "@material-ui/core/FormLabel";
import {ThreadCard} from "./ThreadCard";

export function ThreadListing({data, onClick}){
  return (<div style={{marginTop: '5rem', float: 'left'}}>
    <FormLabel component="legend" style={{marginBottom: '1.25rem', marginLeft: '1rem'}}>Threads</FormLabel>
    {data && data.map((obj) => (
      <ThreadCard key={obj.id} data={obj} onClick={onClick}/>
    ))}
  </div>)
}
