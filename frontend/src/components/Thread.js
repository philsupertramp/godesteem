import React, {useState} from 'react';
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";
import {TextField, Typography} from "@material-ui/core";
import {MessageListing} from "./MessageListing";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import {encryptMessage} from "../lib/encryption";
import {handleKeyEvent} from "../lib/helper";
function updateScroll(elementId){
    let element = document.getElementById(elementId);
    if(element) {
      element.scrollTop = element.scrollHeight;
    }
}
export function Thread({threadObj}){
  const [thread, setThread] = useState(threadObj);
  const [newMessage, setNewMessage] = useState('')

  const [{ data }, doFetch, setPayload] = useDataApi(
    API.API_URLS.thread_list + `${thread.id}/`,
    null,
    null,
    true
  );
  function submitMessage(){
    setPayload({'text': encryptMessage(newMessage)});
    doFetch(`${API.API_URLS.thread_list}${thread.id}/send-message/`);
    setNewMessage('')
  }
  if(data && !thread.participants && data.id === threadObj.id) {
    setThread(data)
    setTimeout(() => updateScroll("message-list"), 200)
  }
  if(data && thread && threadObj.id !== thread.id) {
    doFetch(API.API_URLS.thread_list + `${thread.id}/`)
    setThread(threadObj)
  }
  if(data && thread)
    if(data.all_messages && thread.all_messages)
      if(data.all_messages.length > thread.all_messages.length) {
        setThread({...thread, all_messages: data.all_messages})
        setTimeout(() => updateScroll("message-list"), 200)
      }
  if(thread && !thread.participants) return null
  return (<Container style={{backgroundColor: '#eee'}}>
      <div style={{marginTop: '2.5rem', marginLeft: '2.5rem', float: 'left'}}>
        <Typography variant="h4">{thread.name}</Typography>
        <MessageListing messages={thread.all_messages} />
      </div>
        <div style={{float: 'left', marginLeft: '25rem', width: '50rem'}}>
          <form onSubmit={event => {
            event.preventDefault();
            submitMessage()
          }}>
            <TextField
              id="text"
              label="Message"
              placeholder="Message"
              multiline
              style={{width: '30rem'}}
              InputLabelProps={{
                shrink: true,
              }}
              value={newMessage}
              onChange={(e) => setNewMessage(e.target.value)}
              onKeyPress={(e) => handleKeyEvent(e, submitMessage)}
            />
            <Button type={'submit'}>Send</Button>
          </form>
        </div>
  </Container>)
}
