import React from 'react';
import {TextField} from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import {SidekickListing} from "./SidekickListing";



export function UserDetail({user, setUser, disabled}) {
  return (<>
    <h2>
      {user.username}
    </h2>
    <img src={user.profile_image_url} alt={user.username} style={{float: 'left', maxWidth: '15rem', flex: 1}} /><br/>
    <form style={{width: '25%', float: 'left', backgroundColor: '#f5f5f5', padding: 'calc(2 * 0.25rem)', margin: 'calc(-4 * 0.25rem)'}}>
      <FormControl component="fieldset">
        <FormLabel component="legend">Profile</FormLabel>
        <FormGroup>
          <TextField
            disabled={disabled}
            id="first-name"
            label="First name"
            margin="dense"
            variant="outlined"
            value={user.first_name}
            onChange={(e) => setUser({...user, first_name: e.target.value})}
          />
          <TextField
            disabled={disabled}
            id="last-name"
            label="Last name"
            margin="dense"
            variant="outlined"
            value={user.last_name}
            onChange={(e) => setUser({...user, last_name: e.target.value})}
          />
          <TextField
            disabled={disabled}
            id="email"
            label="Email address"
            margin="dense"
            variant="outlined"
            value={user.email_address}
            onChange={(e) => setUser({...user, email: e.target.value})}
          />
        </FormGroup>
      </FormControl>
    </form>
    <SidekickListing data={user.sidekicks}/>
  </>)
}
