import React, {useState, Fragment} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {BASE_URL, API} from "../lib/api";
import {useDataApi} from "../lib/hooks";


export default function PostModal({openModal, handleClose, addPost}) {
    const [content, setContent] = useState(null);
    const [{ data, isLoading, isError }, doFetch, setPayload] = useDataApi(
        ``,
        null,
      null,
      false
    );
    if(data && data.id) addPost(data);
    return (
        <Dialog open={openModal} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth={'lg'} fullWidth>
            <DialogTitle id="form-dialog-title">Create new Post</DialogTitle>
            <Fragment>
                <form
                    onSubmit={event => {
                        setPayload({content});
                        doFetch(`${BASE_URL}${API.API_URLS.post_list}`);
                        event.preventDefault();
                    }}>
                    <DialogContent>
                        <TextField
                            multiline
                            autoFocus
                            margin="dense"
                            id="content"
                            label="Content"
                            type="text"
                            fullWidth
                            onChange={(event) => setContent(event.target.value)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button type='submit' onClick={handleClose} color="primary">
                            Post
                        </Button>
                    </DialogActions>
                </form>
                {isError && <div>Something went wrong ...</div>}

                {isLoading ? (
                    <div>Loading ...</div>
                ) : null}
            </Fragment>
        </Dialog>
    );
}
