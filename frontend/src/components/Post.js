import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import {Grid} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ArrowUpwardSharp from '@material-ui/icons/ArrowUpwardSharp';
import ArrowDownwardSharp from '@material-ui/icons/ArrowDownwardSharp';
import {Link} from "./common/Link";
import {displayDate} from "../lib/helper";
import {useDataApi} from "../lib/hooks";
import {API, api} from "../lib/api";


const useStyles = makeStyles(theme => ({
  grid: {
    float: 'left'
  },
  card: {
    wordBreak: 'break-word',
    float: 'left',
    height: '100%',
    borderColor: '#ccc',
    margin: theme.spacing(1)

  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function Post({data, wide}) {
  const classes = useStyles();
  const createdAt = new Date(data.created_at);
  const editedAt = new Date(data.edited_at);

  const [upVotes, setUpVotes] = useState(data.up_vote_count);
  const [downVotes, setDownVotes] = useState(data.down_vote_count);

  const [{ fetchedData, isLoading}, doFetch, setPayload] = useDataApi(
      '',
      null,
      null,
      false
  );

  const [userVotedUp, setUserVotedUp] = useState(false);
  const [userVotedDown, setUserVotedDown] = useState(false);
  const [triggerVote, setTriggerVote] = useState(false);
  function upVote(){
    // call api to set upvote + 1
    // if success increment
    console.log('UPVOTE')
    setUpVotes(upVotes + 1);
    setPayload({vote: +1});
    setTriggerVote(true);
    if(userVotedDown) {
      setDownVotes(downVotes - 1);
      setUserVotedDown(false);
    }
    setUserVotedUp(true);
  }
  function downVote() {
    // call api to set downvote + 1
    // if success increment
    console.log('DOWNVOTE')
    setDownVotes(downVotes + 1);
    setPayload({vote: -1});
    setTriggerVote(true);
    if(userVotedUp) {
      setUpVotes(upVotes - 1);
      setUserVotedUp(false);
    }
    setUserVotedDown(true);
  }
  if(triggerVote){
    doFetch(API.API_URLS.post_list + `${data.id}/vote/`);
    setTriggerVote(false);
  }

  return (
    <Grid className={classes.grid} item component={'div'} container sm={12} md={wide ? 12 : 6}>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="subtitle1" color="textSecondary" gutterBottom>
            <Button size="small"><Link color={'#787878'} to={`/users/${data.owner.id}`}>{data.owner.username}</Link></Button> at {displayDate(createdAt)}
          </Typography>
          {data.editor && <Typography className="h5" color="textSecondary" gutterBottom>
            <Button size="small"><Link to={`/users/${data.editor.id}`} color={'#787878'}>{data.editor.username}</Link></Button> at {displayDate(editedAt)}
          </Typography>}
          <Typography variant="h4" color="textPrimary">
            {data.content}
          </Typography>
        </CardContent>
        <CardActions>
          <div style={{width: '50%'}}>
          <Button style={{float: 'left'}} size="small"><Link color={'#787878'} to={`/posts/${data.id}`}>See details</Link></Button>
          </div>
          <div style={{width: '40%'}}>
            <Button disabled={userVotedUp} style={{float: 'right', width: '55%'}} onClick={() => upVote()}><ArrowUpwardSharp/><span style={{width: '50%'}}>{upVotes}</span></Button>
            <Button disabled={userVotedDown} style={{float: 'right', width: '55%'}} onClick={() => downVote()}><ArrowDownwardSharp/><span style={{width: '50%'}}>{downVotes}</span></Button>
          </div>
          <div style={{width: '10%', float: 'right', textAlign: 'center'}}>
            {upVotes - downVotes}
          </div>
        </CardActions>
      </Card>
    </Grid>
  );
}
