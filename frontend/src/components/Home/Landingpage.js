import React from 'react'
import backgroundImage from '../../assets/background.jpg'
import logo from '../../assets/logo.png'
import {Typography} from "@material-ui/core";
import Box from "@material-ui/core/Box";

export function Landingpage() {
  const style = {
    backgroundImage: `url(${backgroundImage})`,
    backgroundPositionX: 'center',
    backgroundPositionY: '60%',
    backgroundSize: '100%/auto',
    position: 'fixed',
    left: 0,
    maxWidth: 1920,
    overflow: 'hidden',
    width: '100%',
    height: '100%'
  };
  return (
    <>
      <div style={style}>
        <div style={{backgroundColor: 'rgba(14,14,14,0.51)', width: '100%', maxHeight: '100%', height: '100%'}}>
          <img alt={'logo'} src={logo} style={{maxWidth: '10rem', marginLeft: '1rem', marginTop: '1rem'}} />
          <Box display={{ xs: 'none', md: 'block' }}>
            <div style={{position: 'absolute', bottom: '15rem', right: '10rem'}}>
              <Typography variant='h3' style={{color: '#ccc'}}>Write your thoughts</Typography>
              <Typography variant='h3' style={{color: '#ccc'}}>Send your messages</Typography>
            </div>
          </Box>
          <Box display={{ xs: 'block', md: 'none' }}>
            <div style={{position: 'absolute', left: '5rem', bottom: '15rem', }}>
              <Typography variant='h3' style={{color: '#ccc'}}>Write your thoughts</Typography>
              <Typography variant='h3' style={{color: '#ccc'}}>Send your messages</Typography>
            </div>
          </Box>
        </div>
      </div>
    </>
  )
}