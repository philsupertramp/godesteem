import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles(theme => ({
  card: {
    display: 'flex',
    width: '25rem'
  },
  details: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flex: '1 0 auto',
    width: '18.5rem'
  },
  cover: {
    width: '5rem',
    height: '5rem'
  },
  fullName: {
    width: '12.5rem',
    float: 'left'
  },
  actionButtons: {
    maxWidth: '7.5rem',
    float: 'right',
  }
}));

export function UserCard({data, button, username}) {
  const classes = useStyles();
  const user = username ? data.author : data
  return (
    <Card className={classes.card}>
      <Link to={`/users/${user.id}`}>
        <CardMedia
          component={Avatar}
          className={classes.cover}
          image={user.profile_image_url}
          title={user.username}
        />
      </Link>
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <div className={classes.fullName}>
            <Typography component="h5" variant="h5">
              <Link to={`/users/${user.id}`} style={{textDecoration: 'none'}}>
                <Typography variant={'h6'} color={'primary'}>{user.username}</Typography>
              </Link>
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              {user.first_name} {user.last_name}
            </Typography>
          </div>
          <div className={classes.actionButtons}>
            {button}
          </div>
        </CardContent>
      </div>
    </Card>
  )
}