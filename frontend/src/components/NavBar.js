import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import LoginModal from "./LoginModal";
import {Typography} from "@material-ui/core";
import MessageIcon from "@material-ui/icons/Message";
import AddIcon from "@material-ui/icons/Add";
import {Link} from "./common/Link";
import {getCookie, removeCookie} from "../lib/auth";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

function userLoggedIn() {
  const token = getCookie('JWT')
  return !!token;

}

export default function NavBar() {
  const classes = useStyles();
  const [loggedIn, setLoggedIn ] = useState(false)
  const [loginOpen, setOpen] = useState(false);
  const token = getCookie('JWT')
  if(token && !loggedIn) setLoggedIn(userLoggedIn());

  function handleCloseLoginModal() {
    setOpen(false);
  }
  function logoutUser() {
    removeCookie('JWT');
    window.location.replace('/');
  }
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <Link color={'#ccc'} to={'/'}>Home</Link>
          </Typography>
          {loggedIn && <>
            <Button color="inherit"><Link to={'/sidekicks'} color={'white'}><AddIcon/></Link></Button>
            <Button color="inherit"><Link to={'/messenger'} color={'white'}><MessageIcon/></Link></Button>
          </>}
          <Button onClick={() => loggedIn ? logoutUser() : setOpen(!loginOpen)} color="inherit">{loggedIn ? 'Logout' : 'Login'}</Button>
        </Toolbar>
      </AppBar>
      <LoginModal openModal={!loggedIn && loginOpen} handleClose={handleCloseLoginModal}/>
    </div>
  );
}
