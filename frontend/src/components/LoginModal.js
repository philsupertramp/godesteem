import React, {useState, Fragment} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {API, BASE_URL} from "../lib/api";
import {useDataApi} from "../lib/hooks";
import {setCookie} from "../lib/auth";

const loginUser = ({token}) => {
  setCookie('JWT', token)
  window.location.reload()
}

export default function LoginModal({openModal, handleClose}) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [token, setToken] = useState(null);
  const [{ data, isLoading, isError }, doFetch, setPayload] = useDataApi(
    ``,
    { token: null},
    null,
    true
  );

  if(data && data.token && !openModal && !token){
    loginUser(data);
    setToken(data);
  }
  return (
    <Dialog open={openModal} onClose={handleClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <Fragment>
          <form
            onSubmit={event => {
              setPayload({username, password});
              doFetch(`${BASE_URL}${API.NON_API_URLS.auth}`);
              event.preventDefault();
            }}>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="username"
                autoComplete='username'
                type="text"
                fullWidth
                onChange={(event) => setUsername(event.target.value)}
              />
              <TextField
                autoFocus
                margin="dense"
                id="password"
                label="Password"
                type="password"
                autoComplete={'current-password'}
                fullWidth
                onChange={(event) => setPassword(event.target.value)}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button type='submit' onClick={handleClose} color="primary">
                Login
              </Button>
            </DialogActions>
          </form>
          {isError && <div>Something went wrong ...</div>}

          {isLoading ? (
            <div>Loading ...</div>
          ) : null}
        </Fragment>
    </Dialog>
  );
}
