import React, {useState} from 'react';
import {Grid, TextField} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import {Link} from "./common/Link";
import {displayDate, displayTime, handleKeyEvent} from "../lib/helper";
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2, 1),
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    card: {
        width: '100%',
    },
    comment: {
        marginTop: '2rem'
    },
    submitButton: {
        marginTop: '2rem',
        backgroundColor: '#00d044'
    },
    commentInput: {
        backgroundColor: '#ccc',
        marginTop: '2rem',
        borderRadius: '0.25rem',
        position: 'fixed',
        bottom: 0
    }
}));

function Comment({comment}) {
    const classes = useStyles();
    const createdAt = new Date(comment.created_at);
    const editedAt = new Date(comment.edited_at);

    return (
        <Grid className={classes.comment} item component={'div'} container sm={12} md={12}>
            <Card className={classes.card}>
                <CardContent>
                    {comment.owner && <Typography variant="subtitle1" color="textSecondary" gutterBottom>
                        <Button size="small"><Link color={'#787878'} to={`/users/${comment.owner.id}`}>{comment.owner.username}</Link></Button> at {displayDate(createdAt)} {displayTime(createdAt)}
                    </Typography>}
                    {comment.editor && <Typography className="h5" color="textSecondary" gutterBottom>
                        <Button size="small"><Link to={`/users/${comment.editor.id}`} color={'#787878'}>{comment.editor.username}</Link></Button> at {displayDate(editedAt)} {displayTime(editedAt)}
                    </Typography>}
                    <Typography variant="h4" color="textPrimary" style={{float: 'right'}}>
                        {comment.content}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}


export function CommentListing({comments, postId}){
    const classes = useStyles();
    const [newComment, setNewComment] = useState('');
    let newId = 1
    if(comments.length > 0)
        newId = comments[comments.length - 1].id + 1;
    const [{ data }, doFetch, setPayload] = useDataApi(
        '',
        null,
        null,
        true
    );
    function appendComments(payload){
        comments.push(payload)
    }
    function submitComment(){
        const payload = {'content': newComment, 'post_id': postId, created_at: new Date(), id: newId}
        setPayload(payload);
        appendComments(payload);
        setNewComment('')
        // doFetch(`${API.API_URLS.thread_list}${thread.id}/send-message/`);
    }

    return (<div style={{marginTop: '2.5rem', float: 'left', width: '100%', overflowY: 'auto'}} id={'comment-list'}>
        {comments && comments.map((obj) => (
            <Comment key={obj.id} comment={obj}/>
        ))}
        <form onSubmit={event => {
            event.preventDefault();
            submitComment()
        }}
              className={classes.commentInput}>
            <TextField
                id="text"
                label="Comment"
                placeholder="Comment"
                multiline
                style={{width: '65rem', margin: '2rem'}}
                InputLabelProps={{
                    shrink: true,
                }}
                value={newComment}
                onChange={(e) => setNewComment(e.target.value)}
                onKeyPress={(e) => handleKeyEvent(e, submitComment)}
            />
            <Button className={classes.submitButton} type={'submit'}>Send</Button>
        </form>
    </div>)
}
