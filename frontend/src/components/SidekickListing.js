import React from 'react';
import {UserCard} from "./UserCard";
import FormLabel from "@material-ui/core/FormLabel";
import {MessageButton} from "./common/Buttons";

export function SidekickListing({data, reduced, onClick}){
  console.log(typeof (onClick))
  return (<div style={reduced ? {marginTop: '5rem'} : {width: '50%', float: 'right'}}>
    <FormLabel component="legend" style={{marginBottom: '1.25rem', marginLeft: '1rem'}}>Sidekicks</FormLabel>
    {data && data.map((obj) => (
      <UserCard key={obj.id} data={obj} reduced={reduced} onClick={onClick} button={<MessageButton/>}/>
    ))}
  </div>)
}
