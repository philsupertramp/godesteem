import {SidekickSearch} from "./SidekickSearch";
import {SidekickListing} from "./SidekickListing";
import React, {useState} from "react";
import {useDataApi} from "../lib/hooks";
import {API} from "../lib/api";

export function SidekickView(){
  const [user, setUser] = useState(null);
  const [{ data, isLoading, isError }] = useDataApi(
    API.API_URLS.user_list + `me/`,
    null,
    null,
    true
  );
  if(isError) {
    console.log('ERR')
    return null
  }
  if(data && !user)
    setUser(data)
  return (
    <>
      {user && !isLoading ?
        <>
          <SidekickSearch currentUser={user.id}/>
          <SidekickListing data={user.sidekicks} reduced/>
        </>
        : 'Loading ...'
      }
    </>
  )
}