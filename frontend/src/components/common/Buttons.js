import {Link} from "./Link";
import MessageIcon from "@material-ui/icons/Message";
import CheckIcon from "@material-ui/icons/Check";
import Fab from "@material-ui/core/Fab";
import React from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({}))

export function MessageButton() {
  const classes = useStyles();
  return (
    <Fab color="primary" aria-label="add" className={classes.fab}>
      <Link color={'#ccc'} to={'/messenger'}><MessageIcon style={{color: '#ccc'}} /></Link>
    </Fab>
  )
}

export function AcceptSidekickRequestButton({requestId, onClick}){
  const classes = useStyles();
  return (
    <Fab onClick={()=>onClick(requestId)} color="secondary" aria-label="add" className={classes.fab}>
      <CheckIcon style={{color: 'black'}}/>
    </Fab>
  )
}