import React from 'react';
import {Link as RouterLink} from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";

const useStyles = () => withStyles({
  link: {
    textDecoration: 'none',
  }
})


export function Link({to, children, color}){
  const classes = useStyles();
  return (
    <RouterLink className={classes.link} style={{color: color, textDecoration: 'none'}} to={to}>
      {children}
    </RouterLink>
  )
}