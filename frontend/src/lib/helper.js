export function displayDate(str){
    /**
     * displays a date in requested format
     * @type {Date}
     */
    const date = new Date(str);
    const mm = date.getMonth() + 1; // Because JS is shit!
    const dd = date.getDay();

    return [(dd < 10 ? '0' : '') + dd,
        (mm < 10 ? '0' : '') + mm,
        date.getFullYear(),
    ].join('.');
}

export function displayTime(str){
    /**
     * displays time from a datetime obj
     * @type {Date}
     */
    const datetime = new Date(str);
    return datetime.toLocaleTimeString()
}

export function handleKeyEvent(event, fun){
    if(event.key === 'Enter'){
        console.log('ENTER')
        fun()
    }
}
