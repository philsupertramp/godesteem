import axios from 'axios';
import {getCookie} from "./auth";

export const BASE_URL = 'http://api.godesteem.de';

export const NON_API_URLS = {
  auth: '/auth/obtain-token/',
  verify: '/auth/verify/',
};
export const API_URLS = {
  post_list: '/api/posts/',
  post_detail: `/api/posts/{id}/`,
  user_list: '/api/users/',
  user_detail: `/api/users/{id}/`,
  thread_list: '/api/messenger/threads/',
  sidekick_request_list: '/api/sidekick-requests/',
  sidekick_request_accept: '/accept/'
};

export const API = {
    NON_API_URLS, API_URLS
};


export const api = axios.create({
  baseURL: BASE_URL,
  timeout: 1000,
});

api.interceptors.request.use(function (config) {
  const token = getCookie('JWT');
  if(token) {
    config.headers.common['Authorization'] = `JWT ${token}`;
  }

  return config;
});
