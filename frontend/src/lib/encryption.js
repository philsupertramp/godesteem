export function encryptMessage(message) {
 return Buffer.from(message, 'utf-8').toString('base64')
}

export function decryptMessage(message) {
 return Buffer.from(message, 'base64').toString('utf-8')
}