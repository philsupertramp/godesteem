import {useEffect, useState} from "react";
import {api} from "./api";

function handleError(error){
  if (error.response) {
    /*
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  } else if (error.request) {
    /*
     * The request was made but no response was received, `error.request`
     * is an instance of XMLHttpRequest in the browser and an instance
     * of http.ClientRequest in Node.js
     */
    console.log(error.request);
  } else {
    // Something happened in setting up the request and triggered an Error
    console.log(`Error ${error.message}`);
  }
}

export const useDataApi = (initialUrl, initialData, initialPayload, isDetail) => {
  const [data, setData] = useState(initialData);
  const [url, setUrl] = useState(initialUrl);
  const [payload, setPayload] = useState(initialPayload);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    async function fetchData() {
      setIsError(false);
      setIsLoading(true);
      let hasError = false
      if(url && url !== "") {
        let result = null;
        if (payload) {
          result = await api.post(url, payload).catch((error) => {
            setIsError(true);
            handleError(error)
            hasError = true
          });
        } else
          result = await api(url).catch((error) => {
            setIsError(true);
            handleError(error)
            hasError = true
          });
        if(!hasError) {
          if (isDetail || !!payload) setData(result.data);
          else setData(result.data.results);
        }
      }
      setIsLoading(false);
    };

    fetchData();
  }, [url, payload, initialUrl, isDetail]);

  return [{ data, isLoading, isError }, setUrl, setPayload];
};
