import Cookies from 'universal-cookie';
import {decryptMessage, encryptMessage} from "./encryption";
const cookies = new Cookies();

export function setCookie(name, value){
  let val = encryptMessage(value)
  cookies.set(name, val, { path: '/' , maxAge: 365 * 24 * 60 * 60, sameSite: 'Strict'});
}
export function getCookie(name){
  let val = cookies.get(name)
  return val ? decryptMessage(val) : null
}
export function removeCookie(name) {
  return cookies.remove(name)
}