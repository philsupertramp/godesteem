import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

export let theme = createMuiTheme({
    palette: {
        primary: {
            main: '#161616',
            contrastText: '#bebebe'
        },
        secondary: {
            main: '#01FF70'
        }
    }
});
theme = responsiveFontSizes(theme);
