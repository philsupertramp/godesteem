from django.db import models

from userprofile.models import UserProfile
from utils.model_mixins import CreateEditDateTimeModel


class BaseContentClass(CreateEditDateTimeModel):
    up_vote_count = models.PositiveIntegerField(default=0)
    down_vote_count = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True

    @property
    def votes(self):
        return self.up_vote_count - self.down_vote_count


class Post(BaseContentClass):
    owner = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE, related_name='own_posts')
    editor = models.ForeignKey(to=UserProfile, on_delete=models.SET_NULL, null=True, blank=True,
                               related_name='edit_posts')

    content = models.TextField(db_index=True)


class Comment(BaseContentClass):
    owner = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE, related_name='own_comments')

    content = models.TextField(db_index=True, max_length=255)
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE, related_name='comments')


class Vote(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField(default=0)

    class Meta:
        abstract = True


class PostVote(Vote):
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE)
    user = models.ForeignKey(to=UserProfile, on_delete=models.SET_NULL, null=True, related_name='post_votes')


class CommentVote(Vote):
    comment = models.ForeignKey(to=Comment, on_delete=models.CASCADE)
    user = models.ForeignKey(to=UserProfile, on_delete=models.SET_NULL, null=True, related_name='comment_votes')
