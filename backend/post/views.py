from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from post.models import Post, PostVote
from post.serializers import PostSerializer
from userprofile.models import UserProfile


class PostViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin):
    queryset = Post.objects.all().order_by('-created_at')
    serializer_class = PostSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user and self.request.user.is_authenticated:
            user = UserProfile.objects.filter(user=self.request.user)
            return qs.filter(owner__in=user.first().sidekicks.all() | user)
        return qs

    @action(detail=True, methods=['post'], url_path='vote')
    def vote(self, request, pk=None):
        """Vote for a post using the payload \n{\"vote\": -1} for down votes and \n{\"vote\": 1} for up votes"""
        post = self.get_object()
        user = get_object_or_404(UserProfile, user=request.user)
        amount = request.data.get('vote')
        if amount:
            _, created = PostVote.objects.update_or_create(user=user, post=post, defaults={'amount': amount})
            # unfortunately we need to track up and down votes... so we need to calculate them on each vote
            if amount > 0:
                post.up_vote_count += amount
                if not created and post.down_vote_count > 0:
                    post.down_vote_count -= amount
            else:
                post.down_vote_count += abs(amount)
                if not created and post.up_vote_count > 0:
                    post.up_vote_count -= abs(amount)
            post.save()
            return Response({'created': created, 'votes': post.votes},
                            status=status.HTTP_201_CREATED if created else status.HTTP_200_OK)
        raise ValidationError('vote not provided')
