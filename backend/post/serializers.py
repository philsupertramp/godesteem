from rest_framework import serializers

from post.models import Post, Comment
from userprofile.models import UserProfile
from userprofile.serializers import BaseUserProfileSerializer


class CommentSerializer(serializers.ModelSerializer):
    owner = BaseUserProfileSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ['created_at', 'edited_at', 'up_vote_count', 'down_vote_count', 'owner']


class PostSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)
    owner = BaseUserProfileSerializer(read_only=True)
    votes = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = [
            'created_at', 'edited_at', 'up_vote_count', 'down_vote_count',
            'owner', 'editor', 'comments'
        ]

    def create(self, validated_data):
        request = self.context['request']
        user = request.user
        if user:
            validated_data['owner'] = UserProfile.objects.get(user__username=user.username)

        return super().create(validated_data)

    def update(self, instance, validated_data):
        request = self.context['request']
        user = request.user
        if user:
            validated_data['editor'] = UserProfile.objects.get(user=user)

        return super().update(instance, validated_data)
