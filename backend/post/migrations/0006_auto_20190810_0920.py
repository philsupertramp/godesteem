# Generated by Django 2.2.4 on 2019-08-10 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0005_commentvote_postvote'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentvote',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='postvote',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
