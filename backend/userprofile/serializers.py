from rest_framework import serializers
from userprofile.models import UserProfile, SidekickRequests


class BaseUserProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    email_address = serializers.EmailField(source='user.email')
    id = serializers.SerializerMethodField()

    class Meta:
        model = UserProfile
        fields = ['id', 'username', 'first_name', 'last_name', 'email_address', 'profile_image_url']

    def get_id(self, obj):
        request = self.context.get('request', None)
        if request and request.user == obj.user:
            return 'me'
        return obj.id


class UserProfileSerializer(BaseUserProfileSerializer):
    sidekicks = serializers.SerializerMethodField()

    class Meta:
        model = UserProfile
        fields = ['id', 'username', 'first_name', 'last_name', 'email_address', 'sidekicks', 'profile_image_url']

    def get_sidekicks(self, obj):
        sidekicks = obj.sidekicks.all().exclude(user=self.context['request'].user)
        return BaseUserProfileSerializer(sidekicks, many=True).data


class SidekickRequestsSerializer(serializers.ModelSerializer):
    author = BaseUserProfileSerializer()
    receiver = BaseUserProfileSerializer()

    class Meta:
        model = SidekickRequests
        fields = ['id', 'author', 'receiver', 'created_at', 'accepted_at']
