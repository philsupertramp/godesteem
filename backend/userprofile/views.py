from django.db.models import Q
from django.utils import timezone
from rest_framework import mixins, filters, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from userprofile.models import UserProfile, SidekickRequests
from userprofile.serializers import UserProfileSerializer, SidekickRequestsSerializer


class UserViewSet(GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['user__username', 'user__email']

    def get_queryset(self):
        return super().get_queryset().exclude(user=self.request.user)

    def get_object(self):
        """extended functionality to easily retrieve own account"""
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        pk = self.kwargs[lookup_url_kwarg]
        if pk == 'me':
            return self.queryset.get(user=self.request.user)
        return super().get_object()

    @action(detail=True, methods=['post'], url_path='sidekick-request')
    def request_sidekick(self, request, pk=None):
        """request a sidekick connection for the requesting user to in pk specified user"""
        if request.data:
            raise ValidationError('No payload allowed')
        requesting_user = UserProfile.objects.get(user=request.user)
        target_user = self.get_object()
        if requesting_user == target_user:
            raise ValidationError('Can\'t connect with yourself.')
        created = False
        if not target_user.sidekicks.filter(user=requesting_user.user).exists():
            if not SidekickRequests.objects.filter(author=requesting_user, receiver=target_user).exists():
                SidekickRequests.objects.create(author=requesting_user, receiver=target_user)
                created = True
                message = 'requested'
            else:
                message = 'already requested'
        else:
            message = 'already connected'
        stat = status.HTTP_201_CREATED if created else status.HTTP_200_OK
        return Response({'message': message}, status=stat)


class SidekickRequestsViewSet(GenericViewSet, mixins.RetrieveModelMixin, mixins.ListModelMixin):
    serializer_class = SidekickRequestsSerializer
    queryset = SidekickRequests.objects.all()

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return SidekickRequests.objects.none()
        return super().get_queryset().filter(Q(author__user=self.request.user) | Q(receiver__user=self.request.user))

    def get_object(self):
        obj = super().get_object()
        usr = self.request.user
        if obj.author.user == usr or obj.receiver.user == usr:
            return obj
        return None

    @action(detail=True, methods=['post'], url_path='accept')
    def accept(self, request, pk=None):
        """accept sidekick connections"""
        if request.data:
            raise ValidationError('No payload allowed')

        obj = self.get_object()
        requesting_user = request.user
        if obj:
            created = False
            if obj.accepted_at:
                raise ValidationError('Already accepted.')
            elif obj.receiver.user == requesting_user:
                obj.receiver.sidekicks.add(obj.author)
                obj.author.sidekicks.add(obj.receiver)
                obj.accepted_at = timezone.now()
                obj.save(update_fields=['accepted_at'])
                created = True
                message = 'Accepted request.'
            else:
                message = 'Can\'t accept your own request.'
            stat = status.HTTP_201_CREATED if created else status.HTTP_200_OK
            return Response({'message': message}, status=stat)

        raise NotFound()
