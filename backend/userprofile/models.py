from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from utils.model_mixins import CreateEditDateTimeModel


class UserProfile(models.Model):
    user = models.OneToOneField(to=get_user_model(), on_delete=models.PROTECT)
    sidekicks = models.ManyToManyField(to='UserProfile', related_name='other_sidekicks')
    profile_image_url = models.URLField(null=True, blank=True)

    def __str__(self):
        return str(self.user) if self.user else '–'


@receiver(signal=post_save, sender=get_user_model())
def create_profile_on_user_creation(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        UserProfile.objects.get_or_create(user=instance)


class SidekickRequests(CreateEditDateTimeModel):
    author = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE, related_name='outgoing_sidekick_requests')
    receiver = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE, related_name='incoming_sidekick_requests')
    accepted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Sidekick requests'

    def __str__(self):
        return f'{self.author.user.username} –> {self.receiver.user.username} '
