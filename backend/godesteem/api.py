from fcm_django.api.rest_framework import FCMDeviceViewSet
from rest_framework import routers

from messenger.views import ThreadViewSet
from post.views import PostViewSet
from userprofile.views import UserViewSet, SidekickRequestsViewSet

router = routers.SimpleRouter()
router.register(r'posts', PostViewSet)
router.register(r'users', UserViewSet)
router.register(r'sidekick-requests', SidekickRequestsViewSet)
router.register(r'messenger/threads', ThreadViewSet)
router.register(r'devices', FCMDeviceViewSet)


urlpatterns = router.urls
