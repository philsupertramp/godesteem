from rest_framework import serializers

from messenger.models import Thread, Message
from userprofile.serializers import BaseUserProfileSerializer
from rest_framework.fields import (  # NOQA # isort:skip
    empty
)


class MessageSerializer(serializers.ModelSerializer):
    from_me = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ['id', 'created_at', 'content', 'from_me', 'is_read']
        read_only_fields = ['id', 'created_at', 'from_me', 'is_read']

    def get_from_me(self, obj):
        request = self.context.get('request')
        if request:
            user = request.user
            return obj.author.user == user
        return False

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        if not self.context.get('request'):
            del ret['from_me']
        return ret


class BaseThreadSerializer(serializers.ModelSerializer):
    participants = BaseUserProfileSerializer(many=True, required=False)
    name = serializers.SerializerMethodField()

    _participants = None

    class Meta:
        model = Thread
        fields = ['id', 'participants', 'name']

    def get_name(self, obj):
        return obj._name

    def run_validation(self, data=empty):
        self._participants = data.pop('participants', None)
        return super().run_validation(data)

    def create(self, validated_data):
        instance = super().create(validated_data)
        if self._participants:
            [instance.participants.add(p) for p in self._participants]
            instance.save()
        return instance


class ThreadSerializer(BaseThreadSerializer):
    all_messages = MessageSerializer(many=True, read_only=True, source='messages')

    class Meta:
        model = Thread
        fields = ['id', 'participants', 'messages', 'all_messages', 'name']


class ThreadListSerializer(BaseThreadSerializer):
    pass
