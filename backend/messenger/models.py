from django.db import models

from utils.model_mixins import CreateEditDateTimeModel


class Message(CreateEditDateTimeModel):
    author = models.ForeignKey(to='userprofile.UserProfile', on_delete=models.CASCADE, related_name='messages')
    read_at = models.DateTimeField(null=True, blank=True)
    is_read = models.BooleanField(default=False)
    content = models.TextField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.read_at and not self.is_read:
            self.is_read = True
        super().save(force_insert, force_update, using, update_fields)


class Thread(CreateEditDateTimeModel):
    name = models.CharField(max_length=255, null=True, blank=True)
    participants = models.ManyToManyField(to='userprofile.UserProfile', related_name='threads')
    messages = models.ManyToManyField(to=Message, related_name='thread')

    def __str__(self):
        return self._name

    @property
    def _name(self):
        return f'{self.name if self.name else ", ".join(self.participants.all().values_list("user__username", flat=True))}'

    @property
    def is_read(self):
        return not self.messages.filter(is_read=False).exists()
