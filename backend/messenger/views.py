from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.viewsets import GenericViewSet

from messenger.models import Thread, Message
from messenger.serializers import ThreadSerializer, ThreadListSerializer
from userprofile.models import UserProfile


class ThreadViewSet(GenericViewSet,
                    mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'create']:
            return ThreadListSerializer
        return super().get_serializer_class()

    def get_queryset(self):
        user = self.request.user
        qs = super().get_queryset()
        if user.is_authenticated:
            return qs.filter(participants__user=user)
        return qs

    def get_object(self):
        obj = super().get_object()
        obj.messages.exclude(author__user=self.request.user).filter(is_read=False).update(is_read=True)
        return obj

    @action(detail=True, methods=['POST'], url_path='send-message')
    def send_message(self, request, pk=None):
        message_text = request.data.get('text', None)
        if not message_text:
            raise ValidationError('Message empty.')

        thread = self.get_object()
        message = Message.objects.create(author=UserProfile.objects.get(user=request.user), content=message_text)
        thread.messages.add(message)

        # inform recipient
        return self.retrieve(request, pk)
