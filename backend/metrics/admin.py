from django.contrib import admin

# Register your models here.
from metrics.models import RequestLogger


class RequestLoggerAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'url']


admin.site.register(RequestLogger, RequestLoggerAdmin)
