from django.db import models


class RequestLogger(models.Model):
    request_content = models.TextField()
    url = models.URLField()
    response_content = models.TextField()
