from metrics.models import RequestLogger


class RequestLoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request_content = request.body
        response = self.get_response(request)
        url = request.path
        if url.find('/admin') < 0:
            RequestLogger.objects.create(request_content=request_content, response_content=response.content,
                                         url=request.path)
        return response
